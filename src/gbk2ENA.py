#!/usr/bin/env python3.6
# -*- coding: utf-8 -*-

"""
@Author: Melanie Hennart
@PASTEUR_2018
@Python_3.6
"""
#=============================================================================#
""" 
  gbk2ENA: converting Genbank files into EMBL-like files for submission to ENA

  [Version 1.0]

  Copyright (C) 2018  Melanie Hennart, Alexis Criscuolo 


  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation,  either version 3  of the License,  or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,  but
  WITHOUT  ANY  WARRANTY;   without  even  the   implied  warranty  of
  MERCHANTABILITY or  FITNESS FOR  A PARTICULAR  PURPOSE.  See the GNU 
  General Public License for more details.
  
  You should have  received a copy  of the GNU  General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>.


  Contact: 

  Institut Pasteur
  Biodiversity and Epidemiology of Bacterial Pathogens
  Paris, FRANCE

  melanie.hennart@pasteur.fr

  Institut Pasteur
  Bioinformatics and Biostatistics Hub
  C3BI, USR 3756 IP CNRS
  Paris, FRANCE

  alexis.criscuolo@pasteur.fr
"""
#=============================================================================#

from Bio import SeqIO
import os
import argparse

#=== Parameters

parser = argparse.ArgumentParser(prog="gbk2ENA", description="gbk2ENA v.1.0\n\nThis tool converts Genbank files into EMBL-like files for submission to ENA." , formatter_class=argparse.RawTextHelpFormatter)

parser.add_argument("-i", dest="fileInput"   , type=str, required=True,  help="(mandatory) input file in genbank format")
parser.add_argument("-o", dest="fileOutput"  , type=str, required=True,  help="(mandatory) output file name")
parser.add_argument("-p", dest="ProjectId"   , type=str, required=True,  help="(mandatory) project id (PR lines)")

parser.add_argument("-a", dest="Authors"     , type=str, required=False, default="Unknown"    , help='reference authors (RA lines);     default: "Unknown"')
parser.add_argument("-t", dest="Title"       , type=str, required=False, default="N/A"        , help='reference title (RT lines);       default: "N/A"')
parser.add_argument("-s", dest="SeqTopology" , type=str, required=False, default="linear"     , help='sequence topology (ID token 3);   default: "linear"')
parser.add_argument("-m", dest="MoleculeType", type=str, required=False, default="genomic DNA", help='molecule type (ID token 4);       default: "genomic DNA"')
parser.add_argument("-c", dest="DataClass"   , type=str, required=False, default="STD"        , help='data class (ID token 5);          default: "STD"\n(see 3.1 at ftp.ebi.ac.uk/pub/databases/embl/doc/usrman.txt)')
parser.add_argument("-d", dest="TaxoDiv"     , type=str, required=False, default="UNC"        , help='taxonomic division (ID token 6);  default: "UNC"\n(see 3.2 at ftp.ebi.ac.uk/pub/databases/embl/doc/usrman.txt)\n ')

args = parser.parse_args()

InputFile = args.fileInput
OutputFile = args.fileOutput
Temp_File = OutputFile+'.temp'
Projet = args.ProjectId
Authors = args.Authors
Title = args.Title
SeqTopology = args.SeqTopology
MoleculeType = args.MoleculeType
DataClass = args.DataClass
TaxoDiv = args.TaxoDiv

#=== Convert "Genbank" => "Embl"

count = SeqIO.convert(InputFile, "genbank", Temp_File, "embl")

#=== Convert "Embl" => "ENA"

File = open(Temp_File, 'r')
OutFile = open(OutputFile, 'w')
for line in File.readlines() : 
    if line[:2] == 'ID':
        length = line.split('; ')[-1]
        lineID = ["ID   XXX","XXX", SeqTopology, MoleculeType, DataClass, TaxoDiv, length]      
        OutFile.write('; '.join(lineID))
        OutFile.write("XX\n")
        OutFile.write("AC   XXX;\n")
    elif line[:2] == 'AC':
        Info = line.split('   ')[1]
        OutFile.write("AC * _"+Info)
        OutFile.write("XX\n")       
        OutFile.write("PR   Project:"+Projet+";\n")
    elif line[:2] == "KW":  
        OutFile.write("RA   Submitter, "+Authors+";\n")
        OutFile.write("RT   "+Title+";\n")
    elif line[:2] != "OS" and line[:2] !="OC" :    
        OutFile.write(line)

File.close()
OutFile.close()

os.system("rm "+Temp_File)

print("Converted %i records" % count)
     
        
